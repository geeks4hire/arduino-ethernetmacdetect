#include <SPI.h>
#include <Wire.h>
#include <Dhcp.h>
#include <Dns.h>
#include <Ethernet.h>
#include <EthernetClient.h>
#include <EthernetServer.h>
#include <EthernetUdp.h>

#include <EthernetMacDetect.h>

uint8_t mac[MAC_ADDRESS_LENGTH];

void setup() {
  Serial.begin(9600);
  for(int i = 0; i < 5; i++)
  {
    Serial.println(" ");
  }  
  Serial.println("- Starting test for MAC address ROM");
  Serial.println();

  boolean macResult = EthernetMacDetect.getMacAddressFromEeprom(mac);
  
  if (macResult) {
    Serial.println("   FOUND EEPROM!");  
    Serial.println();      

    Serial.println("- Getting MAC Address");
    Serial.println();      
    
    Serial.print("   [EUI-48 bit MAC] = ");
    char tmpBuf[17];
    sprintf(tmpBuf, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    Serial.println(tmpBuf);
    Serial.println("   TEST OK!");
    Serial.println();
    
    Serial.println("- Now attempting to obtain IP Address via DHCP");    
    Serial.println();
    
    int dhcpResult = Ethernet.begin(mac);    
    if (dhcpResult == 1) {
      Serial.print("   SUCCESSFUL - IP ADDRESS = ");
      Serial.println(Ethernet.localIP());  
      Serial.print("              - NETMASK    = ");
      Serial.println(Ethernet.subnetMask());
      Serial.print("              - GATEWAY IP = ");
      Serial.println(Ethernet.gatewayIP());    
    } else {
      Serial.println("   FAILED - NO VALID RESPONSE TO DHCP REQUEST");
    }  
    Serial.println();  
  } else {
    Serial.print("   UNABLE TO FIND MAC EEPROM ON I2C BUS [ADDRESS: 0x");
    Serial.print(MAC_EEPROM_I2C_ADDRESS, HEX);
    Serial.println("]");
  }
  Serial.println();
}

void loop() {
  while (1) {
    // Nothing to do here...
  }
}
